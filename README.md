Introduction
============

Crawler Rate Limit allows you to limit requests performed by web crawlers, bots,
and spiders. Optionally, it can also rate limit regular traffic.

There are a number of crawlers, bots, and spiders that are known for excessive
crawling of websites. Such requests increase the server load and often affect
performance of the website for regular visitors. In extreme cases they can even
bring the site down.

This module detects if the request is made by a crawler/bot/spider by
inspecting the User-Agent HTTP header and then limits the number of requests
the crawler is allowed to perform in a given time interval. Once the limit is
reached, the server will respond with HTTP code 429 (Too many requests). The
crawler is unblocked at the end of the time interval and a new cycle beings.
If the crawler exceeds the rate limit again, it will again be blocked for the
duration of the same time interval.

**Features**
- by default targets only web crawlers, bots and spiders; regular browser
  traffic is not affected
- number of allowed requests in a given time interval (limit) is configurable
- limits bot requests based on the User-Agent string - if the same crawler uses
  multiple IP addresses, all those requests will count towards the single limit
- optionally rate limits regular traffic (human visitors and bots not openly
  identifying as bots) at the visitor-level (IP address + User-Agent string)
  and / or [autonomous system](https://en.wikipedia.org/wiki/Autonomous_system_(Internet))-level
- limits on regular traffic can be configured independently from the limit for
  bot traffic
- minimal effect on performance
  - rate limiting is performed in a very early phase of request handling
  - uses Redis, APCu or Memcached as rate limiter backend

Requirements
============

Dependencies installed along with the module:
- jaybizzle/crawler-detect - https://github.com/jaybizzle/crawler-detect
- nikolaposa/rate-limit - https://github.com/nikolaposa/rate-limit

Each backend has additional dependencies which must be installed in addition to
the module in order to use that particular backend.

**Redis backend**
- Either Redis PECL extension (https://pecl.php.net/package/redis) or Predis PHP
  package (https://github.com/predis/predis)
- Redis Drupal module - https://www.drupal.org/project/redis

**APCu backend**
- APCu PECL extension - https://pecl.php.net/package/APCu

**Memcached backend**
- Memcached PECL extension - https://pecl.php.net/package/memcached
- Memcache Drupal module - https://www.drupal.org/project/memcache

**Important:** Crawler Rate Limit does not support Mecache PECL extension. Only
Memcached is supported.

**Autonomous system (ASN)-level limiting (optional)**
- geoip2/geoip2 composer package - https://packagist.org/packages/geoip2/geoip2
- GeoLite2/GeoIP2 binary ASN Database - https://dev.maxmind.com/geoip/docs/databases/asn#binary-database
- Cron task or GeoIP Update to keep the ASN database up-to-date - https://dev.maxmind.com/geoip/updating-databases

Installation and configuration
==============================

First install dependencies for your backend. Follow the steps covering your
backend of choice.

APCu backend
------------

1. Install APCu PECL extension by following instructions provided for your
   operating system.

Redis backend
-------------

1. Install Redis PECL extension by following instructions provided for your
   operating system or install Predis PHP package via composer. It's sufficient
   to install one or the other.

2. Install Drupal Redis module

3. Configure Redis module. Crawler Rate Limit requires only minimum Redis
   configuration (host and port). It's not necessary to configure cache, locking
   or any other Redis backend.

    ```php
    // Example Redis configuration.
    $settings['redis.connection']['host'] = '127.0.0.1';
    $settings['redis.connection']['port'] = '6379';
    ```

Memcache backend
----------------

1. Install Memcached PECL extension by following instructions provided for your
   operating system.

2. Install Drupal Memcache module

3. Configure Memcache module. Crawler Rate Limit requires only minimum Memcache
   configuration (servers and bins). It's not necessary to configure cache,
   locking or any other Memcache backend.

    ```php
    // Example Memcache configuration.
    $settings['memcache']['servers'] = ['127.0.0.1:11211' => 'default'];
    $settings['memcache']['bins'] = ['default' => 'default'];
    $settings['memcache']['key_prefix'] = '';
    ```

Autonomous system (ASN)-level limiting
--------------------------------------

_Proceed with this section only if you intend to enforce ASN-based regular
traffic limits._

1. Install `geoip2/geoip2`

   ```sh
   composer require geoip2/geoip2
   ```

2. Place a copy of the GeoLite2/GeoIP2 binary ASN Database into a location
   on the server that is accessible by Drupal
   1. Register for a free or paid MaxMind.com account
   2. Download the GeoLite2 (free) or GeoIP2 (paid) binary ASN Database
   3. Upload to server.

3. Schedule a cron task or GeoIP Update to keep the ASN database up-to-date
   (IP address <-> ASN data changes over time)


Once the backend (and ASN, if using) dependencies are in place, you can install
Crawler Rate Limit.

Install Crawler Rate Limit module
---------------------------------

1. Crawler Rate Limit manages its dependencies via composer. Just copying the
   module into the modules folder won't work.

    ```sh
    cd /root/of/your/drupal/project
    composer require drupal/crawler_rate_limit
    drush en crawler_rate_limit
    ```

2. Configure Crawler Rate Limit. Add the following snippet to your
   `settings.php` file.

    ```php
    // Below configuration uses a redis backend and will limit each
    // crawler / bot (identified by User-Agent string) to a maximum of 100
    // requests every 600 seconds.
    //
    // Regular traffic (human visitors and bots not openly identifying as bots)
    // will be limited to a maximum of 300 requests per visitor
    // (identified by IP address + User-Agent string) every 600 seconds.
    //
    // Regular traffic will additionally be limited at the ASN-level to a
    // maximum of 600 requests per ASN every 600 seconds.
    // See https://en.wikipedia.org/wiki/Autonomous_system_(Internet)

    // Enable or disable rate limiting. Required.
    $settings['crawler_rate_limit.settings']['enabled'] = TRUE;

    // Supported backends: redis, memcached, apcu. Required.
    $settings['crawler_rate_limit.settings']['backend'] = 'redis';

    // Limit for crawler / bot traffic (visitors that openly identify as
    // crawlers / bots). Required.
    $settings['crawler_rate_limit.settings']['bot_traffic'] = [
      // Time interval in seconds. Must be whole number greater than zero.
      'interval' => 600,
      // Number of requests allowed in the given time interval per crawler or
      // bot (identified by User-Agent string). Must be a whole number greater
      // than zero.
      'requests' => 100,
    ];

    // Limits for regular website traffic (visitors that don't openly identify
    // as crawlers / bots). Optional. Omit or set to 0 to disable.
    // Visitor-level (IP address + User-Agent string) regular traffic rate
    // limit.
    $settings['crawler_rate_limit.settings']['regular_traffic'] = [
      // Time interval in seconds. Must be whole number greater than zero.
      'interval' => 600,
      // Number of requests allowed in the given time interval per regular
      // visitor (identified by combination of IP address + User-Agent string).
      'requests' => 300,
    ];
    // Autonomous system-level (ASN) regular traffic rate limit.
    // Requires geoip2/geoip2 and associated ASN Database.
    // See https://github.com/maxmind/GeoIP2-php
    // See https://dev.maxmind.com/geoip/docs/databases/asn#binary-database
    $settings['crawler_rate_limit.settings']['regular_traffic_asn'] = [
      // Time interval in seconds. Must be whole number greater than zero.
      'interval' => 600,
      // Number of requests allowed in the given time interval per autonomous
      // system number (ASN).
      'requests' => 600,
      // Absolute path to the local ASN Database file. Must be an up-to-date,
      // GeoLite2/GeoIP2 binary ASN Database. Consider updating automatically
      // via GeoIP Update or cron.
      // See https://dev.maxmind.com/geoip/updating-databases
      'database' => '/var/www/example.com/private/geoip2/GeoLite2-ASN.mmdb',
    ];
    ```

    - Make sure to adjust the value for `backend` to match the one you decided
      to use.
    - Make sure to adjust the values for `interval` and `requests` according
      to the capabilities of your server and level of the traffic your website
      receives. You may need to review your server logs in order to determine
      optimal values for your server.

3. Visit Status report page (/admin/reports/status) and confirm that Crawler
   Rate Limit is enabled and it doesn't report any warnings or errors.

Test your installation
======================

To validate rate limiting is working, even on setups with edge implementations,
we can use curl to quickly hit the site multiple times. Keep in mind each
request speed will be subject to your connectivity to site, but in most cases,
you can expect about 1 request a second.

To test bot rate limiting, run the following:

```bash
for i in `seq 1 101`; do curl -A "Bytespider" -kLI https://example.com/?i=$i; done
```

Change 101 to one more than your rate limit, and https://example.com/ to the url
of your website where Crawler Rate Limit is installed. If your first requests
are not HTTP/2 200, ensure you don't have "Bytespider" blocked by other means,
or try a different bot User-Agent. Your last response should look something
like:

```
HTTP/2 429
cache-control: no-cache, private
content-type: text/html; charset=UTF-8
date: Sat, 20 Apr 2024 15:05:38 GMT
retry-after: 600
server: Apache/2.4.56 (Debian)
x-powered-by: PHP/8.1.28
```

If you want to validate that the functionality is working across ip addresses,
rerun the code above from another machine with a different IP address. You
should receive `HTTP/2 429` from the other machine as long as you do so within
the configured time limit, and ensure you are using the same User-Agent.

To validate rate limiting is working for visitor-level regular traffic, do the
same as above, but change your User-Agent from `"Bytespider"` to
`"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36"`.
The difference here is if you try this same code from another machine with a
different IP address, each machine will be rate-limited independently.

To validate rate limiting is working for ASN-level regular traffic (across
multiple IPs and user agent strings), run the code from two or more machines
under a single ASN, each with a different IP address and non-bot user agent
string like `"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36"`.  Once the ASN-based regular traffic limit is reached, all machines
under that ASN should begin receiving `HTTP/2 429` responses until the
configured time limit expires.

How to update to version 3
==========================

Updating to version 3 from previous versions of the module requires some changes
to be made to module's settings.

1. Define `['backend']` key and set its value to "redis".
2. Move `['interval']` to `['bot_traffic]['interval']`.
3. Move/rename `['operations']` to `['bot_traffic]['requests']`.

settings.php for version 1 or 2:
```php
$settings['crawler_rate_limit.settings']['enabled'] = TRUE;
$settings['crawler_rate_limit.settings']['operations'] = 100;
$settings['crawler_rate_limit.settings']['interval'] = 600;
```

Equivalent settings.php for version 3:
```php
$settings['crawler_rate_limit.settings']['enabled'] = TRUE;
$settings['crawler_rate_limit.settings']['backend'] = 'redis';
$settings['crawler_rate_limit.settings']['bot_traffic'] = [
  'requests' => 100,
  'interval' => 600,
];
```

Maintainers
===========

- Vojislav Jovanović (vaish) - https://www.drupal.org/u/vaish


Supporting organizations:
- Greentech Renewables - https://www.drupal.org/greentech-renewables
